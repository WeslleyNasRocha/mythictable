# Getting KeyCloak to work with MythicTable

## What is KeyCloak?
Keycloak is an Open Source Identity and Access Management system, which provides JWT tokens to applications to authenitcate users.  KeyCloak supports standard username/password login as well as OAuth2 flows.

## Running MythicTable with KeyCloak
KeyCloak is a drop-in replacement for the current Auth service

### Starting KeyCloak
You will need [Docker](https://www.docker.com) and [Docker Compose](https://docs.docker.com/compose/install/) in order to have KeyCloak run correctly.

Simply run `docker-compose up` within the folder `keycloak` and it will get a fully configured KeyCloak server running and ready to go.

```
$ cd keycloak
$ docker-compose up
```

### Backend
To run the backend so it will authenticate with KeyCloak, use `dotnet run --launch-profile keycloak`

```
$ cd server/src/MythicTable
$ dotnet run --launch-profile keycloak
or
$ dotnet run --project .\server\src\MythicTable\MythicTable.csproj --launch-profile keycloak
```

### Frontend
To run the frontend so it will authenticate using KeyCloak, run `npm run serve -- --mode=keycloak`

```
$ cd html
$ npm run serve -- --mode=keycloak
```
