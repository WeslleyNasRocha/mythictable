import { mount, createLocalVue } from '@vue/test-utils';
import VueRouter from 'vue-router';
import axios from 'axios';
import AxiosMockAdapter from 'axios-mock-adapter';

import CampaignInvitation from '@/views/campaign-management/CampaignInvitation.vue';

const localVue = createLocalVue();
localVue.use(VueRouter);
const router = new VueRouter();

let axiosInstance;

describe('CampaignInvitation', () => {
    beforeEach(function() {
        axiosInstance = new AxiosMockAdapter(axios);
    });

    afterEach(function(done) {
        for (let verb in axiosInstance.handlers) {
            let expectations = axiosInstance.handlers[verb];
            if (expectations.length > 0) {
                for (let expectation of expectations) {
                    done.fail(`Expected URL ${expectation[0]} to have been requested but it wasn't.`);
                }
            }
        }
        axiosInstance.restore();
        done();
    });
    it('sets default data', () => {
        expect(typeof CampaignInvitation.data).toBe('function');
        const defaultData = CampaignInvitation.data();
        expect(defaultData.campaignId).toBe('');
        expect(defaultData.author).toBe('');
        expect(defaultData.title).toBe('');
        expect(defaultData.error).toBe('');
    });
    it('updates data', async done => {
        axiosInstance.onGet('/api/campaigns/1').replyOnce(200, {
            owner: 'Gygax',
            name: 'The Temple of Elemental Doom',
            id: '1',
        });
        const view = mount(CampaignInvitation, { localVue, router });

        const from = {}; // mock 'from' route
        const to = { params: { id: '1' } }; // mock 'to' route
        view.vm.$options.beforeRouteEnter[0](to, from, async callback => {
            callback(view.vm);
            let html = view.html();
            expect(html).toContain('<strong>Gygax</strong>');
            expect(html).toContain('<h1>The Temple of Elemental Doom</h1>');
            done();
        });
    });
    it('updates error', async done => {
        axiosInstance.onGet('/api/campaigns/1').networkErrorOnce();
        const view = mount(CampaignInvitation, { localVue, router });

        const from = {}; // mock 'from' route
        const to = { params: { id: '1' } }; // mock 'to' route
        view.vm.$options.beforeRouteEnter[0](to, from, callback => {
            callback(view.vm);
            expect(view.html()).toContain('Error loading campaign invitation.');
            done();
        });
    });
});
