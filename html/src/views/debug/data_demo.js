const entities = [
    {
        id: 'plusButton',
        asset: { kind: 'image', src: '/static/assets/buttons/Buttons-plus.jpg' },
    },
    {
        id: 'minusButton',
        asset: { kind: 'image', src: '/static/assets/buttons/Buttons-minus.jpg' },
    },
    {
        id: 'undoButton',
        asset: { kind: 'image', src: '/static/assets/buttons/Buttons-undo.jpg' },
    },
    {
        id: 'redoButton',
        asset: { kind: 'image', src: '/static/assets/buttons/Buttons-redo.jpg' },
    },
];

const loader = async store => {
    await store.dispatch('gamestate/entities/load', entities);
};

export { loader, entities };
