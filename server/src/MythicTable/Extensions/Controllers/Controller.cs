using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace MythicTable.Extensions.Controllers
{
    public static class ControllerExtensions
    {
        public static string GetUserId(this ControllerBase controller, IConfiguration configuration)
        {
            if (configuration.GetValue<bool>("MTT_USE_KEYCLOAK"))
            {
                return controller.HttpContext.User.FindFirst("preferred_username")?.Value;
            }
            else
            {
                return controller.HttpContext.User.FindFirst("name")?.Value;
            }
        }
    }
}
