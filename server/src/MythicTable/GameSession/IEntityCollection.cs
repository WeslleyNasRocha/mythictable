﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace MythicTable.GameSession
{
    public interface IEntityCollection
    {
        Task<bool> ApplyDelta(IEnumerable<EntityOperation> operations);

        Task<IEnumerable<object>> GetEntities();
    }
}
