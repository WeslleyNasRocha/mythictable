﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.JsonPatch;
using MythicTable.Campaign.Data;
using MythicTable.Collections.Data;
using Newtonsoft.Json.Linq;

namespace MythicTable.GameSession
{
    public interface ILiveClient
    {
        Task ConfirmDelta(SessionDelta delta);
        Task CharacterAdded(CharacterDTO character);
        Task CharacterRemoved(string characterId);

        //TODO #6: Change the SessionDelta to be the SessionOpDelta
        Task ConfirmOpDelta(SessionOpDelta delta);

        Task Undo();
        Task Redo();

        Task ReceiveDiceResult(RollDTO roll);

        Task ObjectAdded(string collection, JObject obj);
        Task ObjectUpdated(UpdateCollectionHubParameters parameters);
        Task ObjectRemoved(string collection, string id);

        Task ExceptionRaised(string exception);
    }
}
