﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MythicTable.GameSession
{

    // TODO #6, #16: This class should have functionality to apply the JSONPatches coming in to the internal GameState representation
    // The main goal here is persistence of GameState between sessions.
    // The GameState dict should have 2 keys: entities and global. IDs are not necessary on objects, the name of their key serves as ID.
    // TODO #6: It should also support undoing and redoing deltas.
    public class GameState : Dictionary<string, dynamic>, IGameState
    {

        public Task<bool> ApplyDelta(SessionOpDelta delta)
        {
            //TODO
            Console.WriteLine("Apply Delta to GameState");
            return Task.FromResult(true);
        }

        public Task<IEnumerable<dynamic>> GetGameState()
        {
            //TODO
            Console.WriteLine("Get the GameState");
            return Task.FromResult(this.Values.AsEnumerable());
        }

        public Task<bool> Undo(string undoPlaceHolder)
        {
            //TODO
            Console.WriteLine($"Undone: {undoPlaceHolder}");
            return Task.FromResult(true);
        }

        public Task<bool> Redo(string redoPlaceHolder)
        {
            // TODO
            Console.WriteLine($"Redone: {redoPlaceHolder}");
            return Task.FromResult(true);
        }


    }

}
