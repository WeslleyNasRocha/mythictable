﻿using Microsoft.AspNetCore.JsonPatch;
using Microsoft.Extensions.Logging;
using MythicTable.Common.Exceptions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;

namespace MythicTable.Collections.Providers
{
    public class InMemoryCollectionProvider: ICollectionProvider
    {
        private Dictionary<string, List<JObject>> jObjects = new Dictionary<string, List<JObject>>();
        private long lastId = 0;
        private readonly ILogger<InMemoryCollectionProvider> logger;

        public InMemoryCollectionProvider(ILogger<InMemoryCollectionProvider> logger)
        {
            this.logger = logger;
        }

        public Task<JObject> Create(string userId, string collection, JObject jObject)
        {
            var key = GenerateKey(userId, collection);
            if (!jObjects.ContainsKey(key))
            {
                jObjects.Add(key, new List<JObject>());
            }

            jObject["_id"] = (++lastId).ToString();
            jObjects[key].Add(jObject);
            return Task.FromResult(jObject);
        }

        public Task<List<JObject>> Get(string userId, string collection)
        {
            var key = GenerateKey(userId, collection);
            var results = new List<JObject>();
            foreach (KeyValuePair<string, List<JObject>> entry in jObjects)
            {
                if (entry.Key == key || entry.Key.StartsWith($"{key}-"))
                {
                    results.AddRange(entry.Value);
                }
            }
            return Task.FromResult(results);
        }

        public Task<JObject> Get(string userId, string collection, string id)
        {
            var key = GenerateKey(userId, collection);
            if (jObjects.ContainsKey(key))
            {
                try
                {
                    return Task.FromResult(jObjects[key].Single(jObject => jObject["_id"].ToString() == id));
                }
                catch
                {
                }
            }

            var message = $"Could not find item '{id}' in collection '{collection}' for user '{userId}'";
            this.logger.LogError(message);
            throw new MythicTableException(message);
        }

        public Task<int> Delete(string userId, string collection, string id)
        {
            var key = GenerateKey(userId, collection);
            if (jObjects.ContainsKey(key))
            {
                var removed = jObjects[key].RemoveAll(obj => obj["_id"].ToString() == id);

                if (removed > 0)
                {
                    foreach (var (k, v) in jObjects)
                    {
                        if (k.EndsWith(collection))
                        {
                            v.RemoveAll(obj => obj["_id"].ToString() == id);
                        }
                    }
                }

                return Task.FromResult(removed);
            }
            this.logger.LogWarning($"Could not delete item '{id}' in collection '{collection}' for user '{userId}'");
            return Task.FromResult(0);
        }

        public Task<int> Update(string userId, string collection, string id, JsonPatchDocument patch)
        {
            var key = GenerateKey(userId, collection);
            if (jObjects.ContainsKey(key))
            {
                var jObject = jObjects[key].Single(jObject => jObject["_id"].ToString() == id);
                foreach (var operation in patch.Operations)
                {
                    dynamic obj = JsonConvert.DeserializeObject<ExpandoObject>(jObject.ToString());
                    patch.ApplyTo(obj);
                    jObject = JObject.Parse(JsonConvert.SerializeObject(obj));
                }
                jObjects[key].RemoveAll(jObject => jObject["_id"].ToString() == id);
                jObjects[key].Add(jObject);
                return Task.FromResult(1);
            }
            this.logger.LogWarning($"Could not update item '{id}' in collection '{collection}' for user '{userId}'");
            return Task.FromResult(0);
        }

        public async Task<JObject> CreateByCampaign(string userId, string collection, string campaignId, JObject jObject)
        {
            var obj = await Create(CreateCampaignKey(campaignId), collection, jObject);

            var key = GenerateKey(userId, collection);
            if (!jObjects.ContainsKey(key))
            {
                jObjects.Add(key, new List<JObject>());
            }
            jObjects[key].Add(obj);
            return obj;
        }

        public Task<List<JObject>> GetByCampaign(string collection, string campaignId)
        {
            return Get(CreateCampaignKey(campaignId), collection);
        }

        public Task<JObject> GetByCampaign(string collection, string campaignId, string id)
        {
            return Get(CreateCampaignKey(campaignId), collection, id);
        }

        public Task<int> UpdateByCampaign(string collection, string campaignId, string id, JsonPatchDocument patch)
        {
            return Update(CreateCampaignKey(campaignId), collection, id, patch);
        }

        private static string GenerateKey(string userId, string collection)
        {
            return $"{userId}-{collection}";
        }

        private static string CreateCampaignKey(string campaignId)
        {
            return $"campaign-${campaignId}";
        }
    }
}
