﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MythicTable.Campaign.Data;
using MythicTable.Collections.Providers;
using MythicTable.Extensions.Controllers;
using Newtonsoft.Json.Linq;

namespace MythicTable.Collections.Controller
{
    [Route("api/collections")]
    [ApiController]
    [Authorize]
    public class CollectionsController : ControllerBase
    {
        private readonly ICollectionProvider provider;
        private readonly ICampaignProvider campaignProvider;

        private IConfiguration Configuration { get; }

        public CollectionsController(ICollectionProvider provider, ICampaignProvider campaignProvider, IConfiguration configuration)
        {
            this.provider = provider;
            this.campaignProvider = campaignProvider;
            Configuration = configuration;
        }

        [HttpGet("{collection}")]
        public async Task<List<JObject>> Get(string collection)
        {
            return await provider.Get(this.GetUserId(Configuration), collection);
        }

        [HttpPost("{collection}")]
        public async Task<JObject> Post(string collection, JObject jObject)
        {
            return await provider.Create(this.GetUserId(Configuration), collection, jObject);
        }

        [HttpPut("{collection}/id/{id}")]
        public async Task<JObject> Put(string collection, string id, JsonPatchDocument patch)
        {
            if(await provider.Update(this.GetUserId(Configuration), collection, id, patch) > 0)
            {
                return await provider.Get(this.GetUserId(Configuration), collection, id);
            }
            return null;
        }

        [HttpDelete("{collection}/id/{id}")]
        public async Task<JObject> Delete(string collection, string id)
        {
            return new JObject
            {
                {"numberDeleted", await provider.Delete(this.GetUserId(Configuration), collection, id) }
            };
        }

        [HttpGet("{collection}/campaign/{campaignId}")]
        public async Task<List<JObject>> Get(string collection, string campaignId)
        {
            await this.campaignProvider.Get(campaignId);
            return await provider.GetByCampaign(collection, campaignId);
        }


        [HttpPost("{collection}/campaign/{campaignId}")]
        public async Task<JObject> Post(string collection, string campaignId, JObject jObject)
        {
            await this.campaignProvider.Get(campaignId);
            return await provider.CreateByCampaign(this.GetUserId(Configuration), collection, campaignId, jObject);
        }
    }
}
