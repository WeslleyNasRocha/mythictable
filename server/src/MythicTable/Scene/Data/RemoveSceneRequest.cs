namespace MythicTable.Scene.Data
{
    public class RemoveSceneRequest
    {
        public string CampaignId { get; set; }
        public string SceneId { get; set; }
    }
}