using MongoDB.Bson;
using MythicTable.Campaign.Data;

namespace MythicTable.Campaign.Util
{
    public class CharacterUtil
    {
        public static CharacterDTO CreateCharacter(string scene, string image, double q, double r)
        {
            var character = new CharacterDTO();
            character.Token = BsonDocument.Parse($@"{{
                image: {{
                    asset: '.',
                    width: 300,
                    height: 300,
                    origin: {{
                        x: 150,
                        y: 150
                    }}
                }},
                scene: '{scene}',
                pos: {{ q: {q}, r: {r} }}
            }}");
            character.Asset = BsonDocument.Parse($@"{{
                kind: 'image',
                src: '{image}'
            }}");
            character.Attributes = new BsonDocument();
            return character;
        }
        
        public static CharacterDTO CreateColorToken(string scene, string color, double q, double r)
        {
            var character = new CharacterDTO();
            character.Token = BsonDocument.Parse($@"{{
                image: {{
                    color: '{color}'
                }},
                scene: '{scene}',
                pos: {{ q: {q}, r: {r} }}
            }}");
            character.Asset = new BsonDocument();
            character.Attributes = new BsonDocument();
            return character;
        }
    }
}
