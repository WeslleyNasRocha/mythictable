using System.Threading.Tasks;
using MythicTable.Integration.Tests.Util;
using Newtonsoft.Json;
using Xunit;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.JsonPatch;

namespace MythicTable.Integration.Tests
{
    public class CollectionApiTests
    {
        private HttpClient client;

        public CollectionApiTests()
        {
            var builder = new WebHostBuilder().UseStartup<TestStartup>();
            var server = new TestServer(builder);
            client = server.CreateClient();
        }

        [Fact]
        public async Task GetCollectionReturnsEmpty()
        {
            using var response = await RequestHelper.GetStreamAsync(client, "/api/collections/test");
            response.EnsureSuccessStatusCode();
            Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType.ToString());
            var json = await response.Content.ReadAsStringAsync();
            var jObjects = JsonConvert.DeserializeObject<List<JObject>>(json);
            Assert.Empty(jObjects);
        }

        [Fact]
        public async Task PostAndGetReturnsJObjects()
        {
            var o = new JObject
            {
                { "Name", "Integration Test JObject" }
            };
            var collection = "test";

            JObject jObject = await CreateObjectInCollection(collection, o);

            List<JObject> jObjects = await GetCollection(collection);
            Assert.Single(jObjects);
            Assert.Equal("Integration Test JObject", jObjects[0]["Name"]);
        }

        [Fact]
        public async Task UpdateChangesJObjects()
        {
            var o = new JObject
            {
                { "Name", "Integration Test JObject" }
            };
            var collection = "test";

            JObject jObject = await CreateObjectInCollection(collection, o);

            var patch = new JsonPatchDocument()
                            .Replace("Name", "Update Test JObject")
                            .Add("foo", "bar");
            jObject = await UpdateObjectInCollection(collection, patch, jObject["_id"].ToString());

            Assert.Equal("Update Test JObject", jObject["Name"]);
            Assert.Equal("bar", jObject["foo"]);
        }

        [Fact]
        public async Task DeleteApi()
        {
            var collection = "test";
            JObject jObject = await CreateObjectInCollection(collection, new JObject());

            await DeleteObjectInCollection(collection, jObject["_id"].ToString());

            List<JObject> jObjects = await GetCollection(collection);
            Assert.Empty(jObjects);
        }

        private async Task<JObject> CreateObjectInCollection(string collection, JObject o)
        {
            using var response = await RequestHelper.PostStreamAsync(client, $"/api/collections/{collection}", o);
            response.EnsureSuccessStatusCode();
            Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType.ToString());
            var json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<JObject>(json);
        }

        private async Task<List<JObject>> GetCollection(string collection)
        {
            using var response = await RequestHelper.GetStreamAsync(client, $"/api/collections/{collection}");
            response.EnsureSuccessStatusCode();
            Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType.ToString());
            var json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<JObject>>(json);
        }

        private async Task<JObject> UpdateObjectInCollection(string collection, JsonPatchDocument patch, string id)
        {
            using var response = await RequestHelper.PutStreamAsync(client, $"/api/collections/{collection}/id/{id}", patch);
            response.EnsureSuccessStatusCode();
            Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType.ToString());
            var json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<JObject>(json);
        }

        private async Task DeleteObjectInCollection(string collection, string id)
        {
            using var response = await RequestHelper.DeleteStreamAsync(client, $"/api/collections/{collection}/id/{id}");
            response.EnsureSuccessStatusCode();
        }
    }
}
